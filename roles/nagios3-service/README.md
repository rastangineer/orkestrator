nagios3-service
===============

Tasks for setting up nagios3 server.

Requirements
------------

Ubuntu based system.

Role Variables
--------------

nagios3_nagiosadmin_user: nagiosadmin
nagios3_nagiosadmin_pass: nagiosadmin

Dependencies
------------

Not at the moment.

Example Playbook
----------------

To use this role, just include it in your playbook, for example:

    - hosts: servers
      roles:
         - nagios3-service

License
-------

MIT

Author Information
------------------

Please any question, please contact the autor at: jorge.medina@koolops.com.mx.
