snmpd-service
=============

Tasks for setting up local snmpd service.

Requirements
------------

Ubuntu based system.

Role Variables
--------------

cacti_server: 127.0.0.1
ro_community: public

Please set the cacti_server variable on your inventory with the cacti server LAN IP,
for example:

cacti_server=10.1.30.22

Dependencies
------------

Not at the moment.

Example Playbook
----------------

To use this role, just include it in your playbook, for example:

    - hosts: servers
      roles:
         - snmpd-service

License
-------

MIT

Author Information
------------------

Please any question, please contact the autor at: jorge.medina@koolops.com.mx.
