tftp-service
===========

Tasks for setting up local tftp service.

Requirements
------------

Ubuntu based system.

Role Variables
--------------

These are default variables.

tftp_rootpath: /var/lib/tftpboot


Dependencies
------------

Not at the moment.

Example Playbook
----------------

To use this role, just include it in your playbook, for example:

    - hosts: servers
      roles:
         - tftp-service

License
-------

MIT

Author Information
------------------

Please any question, please contact the autor at: jorge.medina@koolops.com.mx.
