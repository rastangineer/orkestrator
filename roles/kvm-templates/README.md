kvm-templates
=============

Tasks for provisioning qemu-kvm server templates.

Requirements
------------

Ubuntu based system.

Role Variables
--------------

# Repository for server images build with packer
server_images_repository: /repositorio/staging/server-images
# Ubuntu server qemu-kvm settings
machine_type: pc-0.12
emulator_path: /usr/bin/kvm
# Memory (default 1GB)
machine_memory: 1048576
# vCPU (default 1)
machine_vcpus: 1
# Ubuntu server virtual machine settings
vm_name: template-ubuntu-16.04-x64-server
template_name: template-ubuntu-16.04-x64-server
images_pool_name: /var/lib/libvirt/images
eth0_network_type: network
eth0_network_name: default
eth0_mac: 52:54:00:e6:a2:7c

Role Tags
---------

 - kvm_templates_full
 - kvm_templates_cleanbefore
 - kvm_templates_create
 - kvm_templates_cleanafter

Dependencies
------------

QEMU/KVM Ubuntu Host.

Example Playbook
----------------

To use this role, just include it in your playbook, for example:

    - hosts: servers
      roles:
         - kvm-templates

License
-------

MIT

Author Information
------------------

Please any question, please contact the author Jorge Armando Medina.
