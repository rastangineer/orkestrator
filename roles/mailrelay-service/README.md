mailrelay-service
=================

Tasks for setup postfix as mail relay service.

Requirements
------------

Ubuntu based system.

Role Variables
--------------

postfix_mynetworks_lan: 127.0.1.1/8
postfix_relay_domain: gmail.com
postfix_relay_server_addr: smtp.gmail.com
postfix_relay_server_port: 587
postfix_relay_server_user: monitoreo@example.com
postfix_relay_server_pass: s3cr3t
postfix_relay_rootalias: lsadmin
postfix_relay_notifications: monitoreo@example.com

Dependencies
------------

Not at the moment.

Example Playbook
----------------

To use this role, just include it in your playbook, for example:

    - hosts: servers
      roles:
         - mailrelay-service

License
-------

MIT

Author Information
------------------

Please any question, please contact the autor at: jorge.medina@koolops.com.mx.
